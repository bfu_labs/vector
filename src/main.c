#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "../inc/matrix.h"

typedef void(*VectorInterpretator)(Vector);

int* getIntPointer(int num){
	int* pt = (int*)malloc(sizeof(int));
	*pt = num;
	return pt;
}

Vector genVector(int size){
	Vector vector = getVector(size);
	int i;
	for (i=0;i<size;i++){
		int* num = getIntPointer(rand()%100);
		setVectorValue(vector, i, num);
	}
	return vector;
}

void destructData(ValueType value){
	free(value);
}

void printVector(Vector vector){
	if (!vector){
		printf("NULL");
		return;
	}
	int size = getVectorSize(vector);
	printf("<");
	int i;
	for (i=0;i<size;i++){
		int* num = (int*)getVectorValue(vector, i);
		if (i!=size-1){
			if (num!=0){
				printf("%d ", *num);
			}else{
				printf("NULL ");
			}
		}else{
			if (num!=0){
				printf("%d", *num);
			}else{
				printf("NULL");
			}
		}
	}
	printf(">");
}

void printMatrix(VectorMatrix matrix, VectorInterpretator interpretator){
	if (!matrix){
		printf("(NULL)");
		return;
	}
	int size = getMatrixSize(matrix);
	printf("{");
	int i;
	for (i=0;i<size;i++){
		Vector vector = (Vector)getMatrixVector(matrix, i);
		if (!vector){
			printf("NULL");
		}else{
			if (!interpretator){
				printf("<*void>");
			}else{
				interpretator(vector);
			}
		}
		printf(" ");
	}
	printf("}");
}

int numComparator(const void* num1, const void* num2){
	if (**(int**)num1>**(int**)num2) return 1;
	return -1;
}

void TestVector(){
	Vector zero = getVector(10);
	printf("Zero Vector generated: \n");
	printVector(zero);
	printf("\n\n");
	Vector vector = genVector(10);
	printf("Vector of size %d, generated:\n", getVectorSize(vector));
	printVector(vector);
	printf("\n\n");
	setVectorValue(vector, 9, getIntPointer(100500));
	printf("Changing 10th vector element:\n");
	printVector(vector);
	printf("\n\n");
	sortVector(vector, numComparator);
	printf("Vector sorted:\n");
	printVector(vector);
	printf("\n\n");
	destructVector(&vector, destructData);
	printf("Vector destructed:\n");
	printVector(vector);
}

void TestMatrix(){
	printf("Empty Vector Matrix Generated:\n");
	VectorMatrix matrix = getMatrix();
	printMatrix(matrix, NULL);
	printf("\n\n");
	printf("Pushing 5 random vectors of size 4:\n");
	int i;
	for (i=0;i<5;i++){
		Vector vector = genVector(4);
		pushVectorToMatrix(matrix, vector);
	}
	printMatrix(matrix, printVector);
	printf("\n\n");
	printf("Getting 3rd vector:\n");
	Vector vector = (Vector)getMatrixVector(matrix, 2);
	printVector(vector);
	printf("\n\n");
	printf("Increasing 2nd and setting 4th element equals -1 in 5th vector:\n");
	(*(int*)getMatrixValue(matrix, 4, 1))++;
	setMatrixValue(matrix, 4, 3, getIntPointer(-1));
	printMatrix(matrix, printVector);
	printf("\n\n");
	printf("Sorting all vectors:\n");
	for (i=0;i<getMatrixSize(matrix);i++){
		Vector vector = (Vector)getMatrixVector(matrix, i);
		sortVector(vector, numComparator);
	}
	printMatrix(matrix, printVector);
	printf("\n\n");
	printf("Matrix destructed:\n");
	destructMatrix(&matrix, destructData);
	printMatrix(matrix, NULL);
}

int main(){
	srand(time(NULL));

	TestVector();
	printf("\n\n\n");
	TestMatrix();
	printf("\n\n\n");

	return 0;
}
