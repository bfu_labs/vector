#include "vector.h"

typedef struct{
	Vector* container;
	int segments;
	int size;
}* VectorMatrix;

VectorMatrix getMatrix();
int getMatrixSize(const VectorMatrix matrix);
void pushVectorToMatrix(VectorMatrix matrix, const Vector vector);
Vector getMatrixVector(const VectorMatrix matrix, const int index);
ValueType getMatrixValue(const VectorMatrix matrix, const int i, const int j);
void setMatrixValue(VectorMatrix matrix, const int i, const int j, ValueType value);
void destructMatrix(VectorMatrix* matrixPt, DataDestructor destructor);
