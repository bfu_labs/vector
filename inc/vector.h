typedef void* ValueType;
typedef int(*Comparator)(const void*, const void*);
typedef void(*DataDestructor)(void*);

typedef struct{
	ValueType* container;
	int size;
}* Vector;

Vector getVector(int size);
int getVectorSize(Vector vector);
void setVectorValue(Vector vector, const int index, const ValueType value);
ValueType getVectorValue(Vector vector, const int index);
void sortVector(Vector vector, const Comparator compare);
void destructVector(Vector* vectorPt, const DataDestructor destructor);
