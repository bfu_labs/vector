.PHONY: all clean

all: bin/test

clean:
	rm -rf *.o
	rm bin/test

bin/test: main.o libContainer.so
	gcc -L. -lContainer -o bin/test main.o -lm

main.o: src/main.c
	gcc -c -I . src/main.c
