#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include "../inc/matrix.h"

const int START_SEGMENT_SIZE = 10;

VectorMatrix getMatrix() {
	VectorMatrix matrix = (VectorMatrix)malloc(sizeof(VectorMatrix));
	int mem_size = START_SEGMENT_SIZE*sizeof(Vector);
	matrix->container = (Vector*)malloc(mem_size);
	memset(matrix->container, 0, mem_size);
	matrix->segments = 1;
	matrix->size = 0;
	return matrix;
}

int getMatrixSize(const VectorMatrix matrix) {
	return matrix->size;
}

int getSegmentSize(int segments){
	return START_SEGMENT_SIZE*pow(2, segments-1);
}

void expandMatrix(VectorMatrix matrix) {
	int mem_size = getSegmentSize(matrix->segments+1)*sizeof(Vector);
	Vector* tmp = (Vector*)realloc(matrix->container, mem_size);
	matrix->container = tmp;
	matrix->segments++;
}

void pushVectorToMatrix(VectorMatrix matrix, const Vector vector) {
	if (matrix->size >= getSegmentSize(matrix->segments)){
		expandMatrix(matrix);
	}
	matrix->container[matrix->size++] = vector;
}

Vector getMatrixVector(const VectorMatrix matrix, const int index){
	return matrix->container[index];
}

ValueType getMatrixValue(const VectorMatrix matrix, const int i, const int j){
	Vector vector = matrix->container[i];
	return getVectorValue(vector, j);
}

void setMatrixValue(VectorMatrix matrix, const int i, const int j, ValueType value){
	Vector vector = matrix->container[i];
	setVectorValue(vector, j, value);
}

void destructMatrix(VectorMatrix* matrixPt, const DataDestructor destructor){
	VectorMatrix matrix = *matrixPt;
	int i;
	for (i=0;i<matrix->size;i++){
		Vector vector = matrix->container[i];
		if (vector){
			destructVector(&vector, destructor);
		}
	}
	free(matrix->container);
	free(matrix);
	*matrixPt = NULL;
}
