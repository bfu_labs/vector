#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "../inc/vector.h"

Vector getVector(int size){
	Vector vector = (Vector)malloc(sizeof(Vector));
	int mem_size = size*sizeof(ValueType);
	vector->container = (ValueType*)malloc(mem_size);
	memset(vector->container, 0, mem_size);
	vector->size = size;
	return vector;
}

int getVectorSize(Vector vector){
	return vector->size;
}

void setVectorValue(Vector vector, const int index, const ValueType value){
	vector->container[index] = value;
}

ValueType getVectorValue(Vector vector, const int index){
	return vector->container[index];
}

void sortVector(Vector vector, const Comparator compare){
	qsort(vector->container, vector->size, sizeof(ValueType), compare);
}

void destructVector(Vector* vectorPt,const DataDestructor destructor){
	Vector vector = *vectorPt;
	int i;
	for (i=0;i<vector->size;i++){
		ValueType value = vector->container[i];
		if (value){
			destructor(value);
		}
	}
	free(vector->container);
	free(vector);
	*vectorPt = NULL;
}
